package com.lyz.oauth2.checker;

import com.lyz.oauth2.extension.server.password.UsernamePasswordGrantAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.AuthorizationGrantType;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/11/10 12:50
 */
public interface AuthenticationChecker {

    void check(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication);

    boolean supports();
}
