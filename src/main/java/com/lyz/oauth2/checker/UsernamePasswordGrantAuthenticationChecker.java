package com.lyz.oauth2.checker;

import com.lyz.oauth2.constant.SecurityConstants;
import com.lyz.oauth2.util.OAuth2Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/11/10 12:50
 */

@Slf4j
public class UsernamePasswordGrantAuthenticationChecker implements AuthenticationChecker {

    private final PasswordEncoder passwordEncoder;

    protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

    public UsernamePasswordGrantAuthenticationChecker(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void check(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) {
        if (!supports()) {
            return;
        }
        Object credentials = authentication.getCredentials();

        String presentedPassword = credentials.toString();
        if (!this.passwordEncoder.matches(presentedPassword, userDetails.getPassword())) {
            log.debug("Failed to authenticate since password does not match stored value");
            throw new BadCredentialsException(this.messages
                    .getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }
    }

    @Override
    public boolean supports() {
        return SecurityConstants.OAUTH_GRANT_TYPE_USERNAME_PASSWORD.equals(OAuth2Utils.getGrantType());
    }
}
