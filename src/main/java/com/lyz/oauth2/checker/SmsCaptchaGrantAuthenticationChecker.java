package com.lyz.oauth2.checker;

import com.lyz.oauth2.constant.SecurityConstants;
import com.lyz.oauth2.util.OAuth2Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 手机验证码授权检查者
 *
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/11/10 12:50
 */

@Slf4j
public class SmsCaptchaGrantAuthenticationChecker implements AuthenticationChecker {

    private final RedisTemplate<String, Object> redisTemplate;

    public SmsCaptchaGrantAuthenticationChecker(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void check(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) {
        if (!supports()) {
            return;
        }
        Object credentials = authentication.getCredentials();
        //校验验证码是否正确
        String mobile = (String) authentication.getPrincipal();
        String code = (String) redisTemplate.opsForValue().get("code:mobile:" + mobile);

//            String presentedCode = (String) credentials;
//            if (!presentedCode.equals(code)) {
//                throw new BadCredentialsException("invalid sms captcha");
//            }
        log.info("验证码类型登录方式，无需校验密码模式");
        // 在这里也可以拓展其它登录方式，比如邮箱登录什么的
    }

    @Override
    public boolean supports() {
        return SecurityConstants.OAUTH_GRANT_TYPE_MOBILE.equals(OAuth2Utils.getGrantType());
    }
}
