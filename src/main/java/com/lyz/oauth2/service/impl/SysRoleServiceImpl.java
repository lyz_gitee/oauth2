/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.lyz.oauth2.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.oauth2.dao.SysRoleMapper;
import com.lyz.oauth2.entity.SysRole;
import com.lyz.oauth2.entity.SysRoleMenu;
import com.lyz.oauth2.service.SysRoleMenuService;
import com.lyz.oauth2.service.SysRoleService;
import com.lyz.oauth2.vo.RoleExcelVO;
import com.lyz.oauth2.vo.RoleVO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2017-10-29
 */
@Service
@AllArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    private SysRoleMenuService roleMenuService;

    /**
     * 通过用户ID，查询角色信息
     *
     * @param userId
     * @return
     */
    @Override
    public List findRolesByUserId(String userId) {
        return baseMapper.listRolesByUserId(userId);
    }

    /**
     * 根据角色ID 查询角色列表，注意缓存删除
     *
     * @param roleIdList 角色ID列表
     * @param key        缓存key
     * @return
     */
    @Override
//	@Cacheable(value = CacheConstants.ROLE_DETAILS, key = "#key", unless = "#result.isEmpty()")
    public List<SysRole> findRolesByRoleIds(List<String> roleIdList, String key) {
        return baseMapper.selectBatchIds(roleIdList);
    }

    /**
     * 通过角色ID，删除角色,并清空角色菜单缓存
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeRoleByIds(String[] ids) {
        roleMenuService
                .remove(Wrappers.<SysRoleMenu>update().lambda().in(SysRoleMenu::getRoleId, CollUtil.toList(ids)));
        return this.removeByIds(CollUtil.toList(ids));
    }

    /**
     * 根据角色菜单列表
     *
     * @param roleVo 角色&菜单列表
     * @return
     */
    @Override
    public Boolean updateRoleMenus(RoleVO roleVo) {
        return roleMenuService.saveRoleMenus(roleVo.getRoleId(), roleVo.getMenuIds());
    }

    /**
     * 查询全部的角色
     *
     * @return list
     */
    @Override
    public List<RoleExcelVO> listRole() {
        List<SysRole> roleList = this.list(Wrappers.emptyWrapper());
        // 转换成execl 对象输出
        return roleList.stream().map(role -> {
            RoleExcelVO roleExcelVO = new RoleExcelVO();
            BeanUtil.copyProperties(role, roleExcelVO);
            return roleExcelVO;
        }).collect(Collectors.toList());
    }
}
