/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.lyz.oauth2.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.oauth2.dao.SysOauthClientDetailsMapper;
import com.lyz.oauth2.entity.SysOauthClientDetails;
import com.lyz.oauth2.service.SysOauthClientDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-15
 */
@Service
@RequiredArgsConstructor
public class SysOauthClientDetailsServiceImpl extends ServiceImpl<SysOauthClientDetailsMapper, SysOauthClientDetails>
        implements SysOauthClientDetailsService {

    /**
     * 根据客户端信息
     *
     * @param clientDetails
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateClientById(SysOauthClientDetails clientDetails) {
        this.insertOrUpdate(clientDetails);
        return Boolean.TRUE;
    }

    /**
     * 添加客户端
     *
     * @param clientDetails
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveClient(SysOauthClientDetails clientDetails) {
        this.insertOrUpdate(clientDetails);
        return Boolean.TRUE;
    }

    /**
     * 插入或更新客户端对象
     *
     * @param clientDetails
     * @return
     */
    private SysOauthClientDetails insertOrUpdate(SysOauthClientDetails clientDetails) {
        // 更新数据库
        saveOrUpdate(clientDetails);
        return clientDetails;
    }

    /**
     * 分页查询客户端信息
     *
     * @param page
     * @param query
     * @return
     */
    @Override
    public Page queryPage(Page page, SysOauthClientDetails query) {
        return baseMapper.selectPage(page, Wrappers.query(query));
    }

    @Override
    public SysOauthClientDetails getClientDetailsById(String clientId) {
        return getOne(Wrappers.<SysOauthClientDetails>lambdaQuery().eq(SysOauthClientDetails::getClientId, clientId));
    }
}
