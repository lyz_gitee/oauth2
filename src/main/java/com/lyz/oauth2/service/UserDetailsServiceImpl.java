package com.lyz.oauth2.service;
import com.google.common.collect.Lists;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.lyz.oauth2.constant.CommonConstants;
import com.lyz.oauth2.constant.SecurityConstants;
import com.lyz.oauth2.core.AuthUser;
import com.lyz.oauth2.dto.UserDTO;
import com.lyz.oauth2.dto.UserInfo;
import com.lyz.oauth2.entity.SysUser;
import com.lyz.oauth2.vo.R;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/10/31 16:24
 */

@Service
public class UserDetailsServiceImpl implements UserDetailsService {


    @Resource
    private SysUserService sysUserService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        R<UserInfo> userInfo = sysUserService.findUserInfoByUserName(username);

        return getUserDetails(userInfo);
    }

    public UserDetails automaticRegisterByMobile(String mobile) throws UsernameNotFoundException {

        UserDTO userDTO = new UserDTO();
        userDTO.setRole(Lists.newArrayList());
        userDTO.setPost(Lists.newArrayList());
        userDTO.setNewPassword("123456");
//        userDTO.setUserId("");
        userDTO.setUsername(mobile);
        userDTO.setPassword("123456");
//        userDTO.setSalt("");
        userDTO.setPhone(mobile);
        userDTO.setAvatar("/admin/sys-file/s3demo/7ff4ca6b7bf446f3a5a13ac016dc21af.png");
        userDTO.setNickname("自动注册用户");
        userDTO.setName("自动注册用户");
//        userDTO.setEmail("");
        userDTO.setDeptId("7");
//        userDTO.setCreateBy("");
//        userDTO.setUpdateBy("");
//        userDTO.setCreateTime(LocalDateTime.now());
//        userDTO.setUpdateTime(LocalDateTime.now());
        userDTO.setLocked("0");
        userDTO.setExpired("0");
        userDTO.setCredentialsExpired("0");
        userDTO.setDisabled("0");
        userDTO.setDeleted("0");
//        userDTO.setWxOpenid("");
//        userDTO.setMiniOpenid("");
//        userDTO.setQqOpenid("");
//        userDTO.setGiteeLogin("");
//        userDTO.setOscId("");


        UserInfo userInfo = sysUserService.saveUser(userDTO);

        return getUserDetails(userInfo);
    }


    /**
     * 构建userdetails
     *
     * @param result 用户信息
     * @return UserDetails
     */
    UserDetails getUserDetails(R<UserInfo> result) {
        if (!result.isSuccess()) {
            throw new UsernameNotFoundException("用户不存在");
        }
        UserInfo info = result.getData();
        return getUserDetails(info);
    }


    /**
     * 构建userdetails
     *
     * @param info 用户信息
     * @return UserDetails
     */
    UserDetails getUserDetails(UserInfo info) {

        Set<String> dbAuthsSet = new HashSet<>();

        if (ArrayUtil.isNotEmpty(info.getRoles())) {
            // 获取角色
            Arrays.stream(info.getRoles()).forEach(role -> dbAuthsSet.add(SecurityConstants.ROLE + role));
            // 获取资源
            dbAuthsSet.addAll(Arrays.asList(info.getPermissions()));

        }

        Collection<GrantedAuthority> authorities = AuthorityUtils
                .createAuthorityList(dbAuthsSet.toArray(new String[0]));
        SysUser user = info.getSysUser();

        // 构造security用户
        return new AuthUser(user.getUserId(),
                user.getDeptId(),
                user.getUsername(),
                user.getPassword(),
                user.getPhone(),
                user.getDisabled().equals("0"),
                user.getExpired().equals("0"),
                user.getCredentialsExpired().equals("0"),
                StrUtil.equals(user.getDeleted(), CommonConstants.STATUS_NORMAL),
                authorities);
    }
}
