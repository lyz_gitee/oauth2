package com.lyz.oauth2.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/11/3 11:54
 */
@ConfigurationProperties(value = "keypair.jks")
@Getter
@Setter
public class KeyPairProperties {
    private String location;
    private String password;
    private String alias;
}
