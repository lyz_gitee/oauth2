package com.lyz.oauth2.config;/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import com.lyz.oauth2.checker.AuthenticationChecker;
import com.lyz.oauth2.checker.SmsCaptchaGrantAuthenticationChecker;
import com.lyz.oauth2.checker.UsernamePasswordGrantAuthenticationChecker;
import com.lyz.oauth2.extension.*;
import com.lyz.oauth2.extension.client.secret.CustomClientSecretAuthenticationProvider;
import com.lyz.oauth2.extension.server.password.UsernamePasswordGrantAuthenticationConverter;
import com.lyz.oauth2.extension.server.password.UsernamePasswordGrantAuthenticationProvider;
import com.lyz.oauth2.extension.server.sms.SmsCaptchaGrantAuthenticationConverter;
import com.lyz.oauth2.extension.server.sms.SmsCaptchaGrantAuthenticationProvider;
import com.lyz.oauth2.handler.AuthenticationFailureEventHandler;
import com.lyz.oauth2.handler.AuthenticationSuccessEventHandler;
import com.lyz.oauth2.handler.MyAccessDeniedHandler;
import com.lyz.oauth2.util.SpringContextHolder;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.OAuth2Token;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configuration.OAuth2AuthorizationServerConfiguration;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configurers.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.oauth2.server.authorization.settings.AuthorizationServerSettings;
import org.springframework.security.oauth2.server.authorization.token.DelegatingOAuth2TokenGenerator;
import org.springframework.security.oauth2.server.authorization.token.OAuth2RefreshTokenGenerator;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;
import org.springframework.security.oauth2.server.authorization.web.authentication.DelegatingAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationConverter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * OAuth Authorization Server Configuration.
 *
 * @author Steve Riesenberg
 */
@Configuration
@RequiredArgsConstructor
@EnableWebSecurity
//主要用于开启在Controller上的@PreAuthorize("@pms.hasPermission('sys_client_add')")生效
@EnableMethodSecurity
public class OAuth2AuthorizationServerSecurityConfiguration {

    private final OAuth2AuthorizationService authorizationService;

    private final RedisTemplate<String, Object> redisTemplate;

    @Value("${keypair.rsa.key-id}")
    private String rsaKeyId;

    private final ResourceAuthExceptionEntryPoint resourceAuthExceptionEntryPoint;

    private final PermitAllUrlProperties permitAllUrl;

    private final PigBearerTokenExtractor pigBearerTokenExtractor;

    private final OpaqueTokenIntrospector opaqueTokenIntrospector;

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public SecurityFilterChain oauth2SecurityFilterChain(HttpSecurity http) throws Exception {
        //授权认证授权SecurityFilterChain
        authorizationServerSecurityFilterChain(http);
        //资源服务认证授权SecurityFilterChain
        resourceServerSecurityFilterChain(http);

        DefaultSecurityFilterChain build = http.build();

        //oauth2集成自定义认证类型
        addCustomOAuth2GrantAuthenticationProvider(http);
        return build;
    }

    private void resourceServerSecurityFilterChain(HttpSecurity http) throws Exception {
        //忽略校验权限
        AntPathRequestMatcher[] requestMatchers = permitAllUrl.getUrls()
                .stream()
                .map(AntPathRequestMatcher::new)
                .collect(Collectors.toList())
                .toArray(new AntPathRequestMatcher[]{});

        http.authorizeHttpRequests(authorizeRequests -> authorizeRequests.requestMatchers(requestMatchers)
                .permitAll()
                .anyRequest()
                .authenticated());

        http.oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(token -> token.introspector(opaqueTokenIntrospector))
                .authenticationEntryPoint(resourceAuthExceptionEntryPoint)
                .bearerTokenResolver(pigBearerTokenExtractor));

        http.headers(headers -> headers.frameOptions(HeadersConfigurer.FrameOptionsConfig::disable));
        http.csrf(AbstractHttpConfigurer::disable);
        //自定义访问权限异常
        http.exceptionHandling(s -> s.accessDeniedHandler(new MyAccessDeniedHandler()));
    }

    private void authorizationServerSecurityFilterChain(HttpSecurity http) throws Exception {
        //授权服务
        OAuth2AuthorizationServerConfigurer authorizationServerConfigurer = new OAuth2AuthorizationServerConfigurer();
        authorizationServerConfigurer.setBuilder(http);
        //设置tokenEndpoint
        authorizationServerConfigurer.tokenEndpoint(tokenEndpoint -> tokenEndpoint
                .accessTokenRequestConverter(accessTokenRequestConverter())
                .accessTokenResponseHandler(new AuthenticationSuccessEventHandler())
                .errorResponseHandler(new AuthenticationFailureEventHandler())
        );
        authorizationServerConfigurer.authorizationServerSettings(providerSettings());
        //设置Token存储Redis
        authorizationServerConfigurer.authorizationService(authorizationService);

        RequestMatcher endpointsMatcher = authorizationServerConfigurer.getEndpointsMatcher();

        authorizationServerConfigurer.clientAuthentication(oAuth2ClientAuthenticationConfigurer ->
                oAuth2ClientAuthenticationConfigurer.errorResponseHandler(new AuthenticationFailureEventHandler()));

        //跨域设置
        http.csrf(csrf -> csrf.ignoringRequestMatchers(new RequestMatcher[]{endpointsMatcher}));
        http.apply(authorizationServerConfigurer);
    }


    /**
     * 注入授权模式实现提供方
     * <p>
     * 1. 密码模式 </br>
     * 2. 短信登录 </br>
     */
    @SuppressWarnings("unchecked")
    private void addCustomOAuth2GrantAuthenticationProvider(HttpSecurity http) {

        OAuth2TokenGenerator<?> tokenGenerator = http.getSharedObject(OAuth2TokenGenerator.class);
        AuthenticationManager authenticationManager = http.getSharedObject(AuthenticationManager.class);
        OAuth2AuthorizationService authorizationService = http.getSharedObject(OAuth2AuthorizationService.class);
        RegisteredClientRepository registeredClientRepository = http.getSharedObject(RegisteredClientRepository.class);
        UserDetailsService userDetailsService = SpringContextHolder.getBean(UserDetailsService.class);

        // 自定义短信认证登录转换器
        SmsCaptchaGrantAuthenticationProvider smsCaptchaGrantAuthenticationProvider = new SmsCaptchaGrantAuthenticationProvider(tokenGenerator, authenticationManager, authorizationService);

        // 自定义用户密码认证登录认证提供
        UsernamePasswordGrantAuthenticationProvider usernamePasswordGrantAuthenticationProvider = new UsernamePasswordGrantAuthenticationProvider(tokenGenerator, authenticationManager, authorizationService);

        // 自定义ClientSecretAuthenticationProvider
        CustomClientSecretAuthenticationProvider customClientSecretAuthenticationProvider = new CustomClientSecretAuthenticationProvider(registeredClientRepository, authorizationService, passwordEncoder());

        // 自定义DaoAuthenticationProvider
        DaoUserDetailsAuthenticationProvider daoUserDetailsAuthenticationProvider = new DaoUserDetailsAuthenticationProvider(passwordEncoder(), userDetailsService, getAuthenticationCheckers());

        http.authenticationProvider(smsCaptchaGrantAuthenticationProvider);
        http.authenticationProvider(usernamePasswordGrantAuthenticationProvider);
        http.authenticationProvider(customClientSecretAuthenticationProvider);
        http.authenticationProvider(daoUserDetailsAuthenticationProvider);
    }


    //自定义认证检查者
    private List<AuthenticationChecker> getAuthenticationCheckers() {
        UsernamePasswordGrantAuthenticationChecker usernamePasswordGrantAuthenticationChecker = new UsernamePasswordGrantAuthenticationChecker(passwordEncoder());
        SmsCaptchaGrantAuthenticationChecker smsCaptchaGrantAuthenticationChecker = new SmsCaptchaGrantAuthenticationChecker(redisTemplate);
        return Arrays.asList(usernamePasswordGrantAuthenticationChecker, smsCaptchaGrantAuthenticationChecker);
    }

    /**
     * request -> xToken 注入请求转换器
     *
     * @return DelegatingAuthenticationConverter
     */
    private AuthenticationConverter accessTokenRequestConverter() {
        return new DelegatingAuthenticationConverter(Arrays.asList(
                new SmsCaptchaGrantAuthenticationConverter(),
                new UsernamePasswordGrantAuthenticationConverter()));
    }


    @Bean
    public JWKSource<SecurityContext> jwkSource(KeyPair keyPair) {
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        // @formatter:off
        RSAKey rsaKey = new RSAKey.Builder(publicKey)
                .privateKey(privateKey)
                .keyID(rsaKeyId)
                .build();
        // @formatter:on
        JWKSet jwkSet = new JWKSet(rsaKey);
        return new ImmutableJWKSet<>(jwkSet);
    }


    @Bean
    public JwtDecoder jwtDecoder(JWKSource<SecurityContext> jwkSource) {
        return OAuth2AuthorizationServerConfiguration.jwtDecoder(jwkSource);
    }

    @Bean
    public JwtEncoder jwtEncoder(JWKSource<SecurityContext> jwkSource) {
        return new NimbusJwtEncoder(jwkSource);
    }

    @Bean
    public AuthorizationServerSettings providerSettings() {
        return AuthorizationServerSettings.builder().issuer("http://www.wangyuanju.com").build();
    }

    /**
     * 配置密码解析器，使用BCrypt的方式对密码进行加密和验证
     *
     * @return BCryptPasswordEncoder
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 自定义Token生成器
     *
     * @param jwtEncoder jwtEncoder
     * @return 自定义Token生成器
     */
    @Bean
    public OAuth2TokenGenerator<OAuth2Token> oAuth2TokenGenerator(JwtEncoder jwtEncoder) {
        JwtOauth2TokenGenerator defaultOAuth2TokenGenerator = new JwtOauth2TokenGenerator(jwtEncoder);
        defaultOAuth2TokenGenerator.setJwtCustomizer(new JwtOAuth2TokenCustomizer());
        return new DelegatingOAuth2TokenGenerator(defaultOAuth2TokenGenerator, new OAuth2RefreshTokenGenerator());

    }
}