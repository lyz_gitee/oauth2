package com.lyz.oauth2.config;


import com.lyz.oauth2.config.properties.KeyPairProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.KeyPair;

@Configuration
@EnableConfigurationProperties({KeyPairProperties.class})
public class KeyPairConfig {
    @Bean
    public KeyPair keyPair(KeyPairProperties pairProperties) {
        ClassPathResource ksFile = new ClassPathResource(pairProperties.getLocation());
        KeyStoreKeyFactory ksFactory = new KeyStoreKeyFactory(ksFile, pairProperties.getPassword().toCharArray());
        return ksFactory.getKeyPair(pairProperties.getAlias());
    }
}