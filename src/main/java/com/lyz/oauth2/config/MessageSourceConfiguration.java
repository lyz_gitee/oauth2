package com.lyz.oauth2.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.util.Locale;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/11/17 20:30
 */
@Configuration
public class MessageSourceConfiguration {
    /**
     * 国际化消息Bean
     *
     * @return 国际化消息Bean
     */
    @Bean
    public MessageSource securityMessageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.addBasenames("classpath:i18n/messages");
        messageSource.setDefaultLocale(Locale.CHINA);
        return messageSource;
    }

}
