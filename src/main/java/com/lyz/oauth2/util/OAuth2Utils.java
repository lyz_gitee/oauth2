package com.lyz.oauth2.util;

import com.lyz.oauth2.constant.SecurityConstants;
import lombok.experimental.UtilityClass;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/11/10 11:29
 */
@UtilityClass
public class OAuth2Utils {


    /**
     * 是否是手机号登录
     *
     * @return true-是 false-是
     */
    public boolean isLoginByMobile() {
        return Objects.equals(getGrantType(), SecurityConstants.OAUTH_GRANT_TYPE_MOBILE);
    }


    public String getGrantType() {
        // 获取当前request
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        // 获取grant_type
        return request.getParameter(OAuth2ParameterNames.GRANT_TYPE);
    }
}
