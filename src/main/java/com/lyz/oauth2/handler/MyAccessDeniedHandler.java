package com.lyz.oauth2.handler;

import com.lyz.oauth2.vo.R;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/11/17 13:20
 */

public class MyAccessDeniedHandler implements AccessDeniedHandler {

    private final MappingJackson2HttpMessageConverter errorHttpResponseConverter = new MappingJackson2HttpMessageConverter();

    private void sendErrorResponse(HttpServletRequest request, HttpServletResponse response,
                                   AccessDeniedException exception) throws IOException {
        ServletServerHttpResponse httpResponse = new ServletServerHttpResponse(response);
        httpResponse.setStatusCode(HttpStatus.FORBIDDEN);
        String errorMessage = "无权限";
        String errorCode = String.valueOf(HttpStatus.FORBIDDEN.value());

        this.errorHttpResponseConverter.write(R.failed(null,errorMessage,errorCode), MediaType.APPLICATION_JSON, httpResponse);
    }
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        // 写出错误信息
        sendErrorResponse(request, response, accessDeniedException);
    }
}
