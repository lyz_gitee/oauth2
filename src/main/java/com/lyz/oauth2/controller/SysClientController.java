/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.lyz.oauth2.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lyz.oauth2.vo.R;
import com.lyz.oauth2.entity.SysOauthClientDetails;
import com.lyz.oauth2.service.SysOauthClientDetailsService;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lengleng
 * @since 2018-05-15
 */
@RestController
@AllArgsConstructor
@RequestMapping("/client")
@Api(value = "客户端管理模块")
public class SysClientController {

    private final SysOauthClientDetailsService clientDetailsService;

    /**
     * 通过ID查询
     *
     * @param clientId clientId
     * @return SysOauthClientDetails
     */
    @GetMapping("/{clientId}")
    @PreAuthorize("@pms.hasPermission('sys_client_add')")
    public R getByClientId(@PathVariable String clientId) {
        SysOauthClientDetails details = clientDetailsService
                .getOne(Wrappers.<SysOauthClientDetails>lambdaQuery().eq(SysOauthClientDetails::getClientId, clientId));
        return R.ok(details);
    }


    /**
     * 添加
     *
     * @param clientDetails 实体
     * @return success/false
     */
//    @SysLog("添加终端")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('sys_client_add')")
    public R add(@Valid @RequestBody SysOauthClientDetails clientDetails) {
        return R.ok(clientDetailsService.saveClient(clientDetails));
    }
}
