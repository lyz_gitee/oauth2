package com.lyz.oauth2.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.UUID;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/10/19 17:11
 */

@RestController
@RequestMapping(value = "/message")
@Slf4j
public class OauthContoller {


    @GetMapping(value = "/read")
    @PreAuthorize("hasAuthority('messages:read')")
    public String read(Principal principal) {
        log.info("principal：{}", principal);
        return UUID.randomUUID().toString();
    }

    @GetMapping(value = "/write")
    @PreAuthorize("hasAuthority('messages:write')")
    public String write(Principal principal) {
        return UUID.randomUUID().toString();
    }
}
