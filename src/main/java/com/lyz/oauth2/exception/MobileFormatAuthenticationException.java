package com.lyz.oauth2.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 认证手机号格式异常
 *
 * @author :  liyuzhi
 * @version :  1.0
 * @createDate : 2023/11/2 15:02
 */
public class MobileFormatAuthenticationException extends AuthenticationException {


    public MobileFormatAuthenticationException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public MobileFormatAuthenticationException(String msg) {
        super(msg);
    }
}
