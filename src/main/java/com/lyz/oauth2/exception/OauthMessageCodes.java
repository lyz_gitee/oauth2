package com.lyz.oauth2.exception;

/**
 * 错误编码
 *
 * @author lengleng
 * @date 2022/3/30
 */
public interface OauthMessageCodes {
//	String OAUTH2_LOGIN_ERROR =	"oauth2.login_error.message"              ;
//	String OAUTH2_SERVER_ERROR             =	"oauth2.server.server_error.message"      ;

    // @formatter:off
	String OAUTH2_LOGIN_USER_NAME_ERROR                   =	    "oauth2.login.user.name_error.message"                           ;
	String OAUTH2_LOGIN_USER_LOCKED                       =	    "oauth2.login.user.locked.message"                               ;
	String OAUTH2_LOGIN_USER_DISABLED                     =	    "oauth2.login.user.disabled.message"                             ;
	String OAUTH2_LOGIN_USER_EXPIRED                      =	    "oauth2.login.user.expired.message"                              ;
	String OAUTH2_LOGIN_USER_CREDENTIALS_EXPIRED          =	    "oauth2.login.user.credentials_expired.message"                  ;
	String OAUTH2_LOGIN_USER_PASSWORD_ERROR  	          =	    "oauth2.login.user.password_error.message"                       ;
	String OAUTH2_LOGIN_CLIENT_ID_ERROR                   =	    "oauth2.login.client.invalid_clientId.message"                   ;
	String OAUTH2_LOGIN_CLIENT_ERROR                      =	    "oauth2.login.client.invalid_client.message"                     ;
	String OAUTH2_LOGIN_CLIENT_SCOPE_ERROR                =	    "oauth2.login.client.invalid_scope.message"                      ;
	String OAUTH2_LOGIN_MOBILE_SMS_CAPTCHA_ERROR          =	    "oauth2.login.mobile.sms_captcha_error.message"                  ;
	String OAUTH2_LOGIN_MOBILE_ERROR			  =	    "oauth2.login.mobile_error.message" 												;
	// @formatter:on
}

