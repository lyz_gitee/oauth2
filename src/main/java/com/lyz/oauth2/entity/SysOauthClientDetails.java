package com.lyz.oauth2.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lyz.oauth2.mybatis.handler.JsonStringArrayTypeHandler;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * 终端信息表
 *
 * @TableName sys_oauth_client_details
 */
@Data
@TableName(value = "sys_oauth_client_details",autoResultMap = true)
public class SysOauthClientDetails implements Serializable {

    /**
     * ID
     */
    @NotNull(message = "[ID]不能为空")
    @ApiModelProperty("ID")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 客户端ID
     */
    @NotBlank(message = "[客户端ID]不能为空")
    @Size(max = 32, message = "编码长度不能超过32")
    @ApiModelProperty("客户端ID")
    @Length(max = 32, message = "编码长度不能超过32")
    private String clientId;

    /**
     * 资源ID集合
     */
    @Size(max = 256, message = "编码长度不能超过256")
    @ApiModelProperty("资源ID集合")
    @Length(max = 256, message = "编码长度不能超过256")
    private String resourceIds;

    /**
     * 客户端秘钥
     */
    @Size(max = 256, message = "编码长度不能超过256")
    @ApiModelProperty("客户端秘钥")
    @Length(max = 256, message = "编码长度不能超过256")
    private String clientSecret;

    /**
     * 授权范围
     */
    @Size(max = 256, message = "编码长度不能超过256")
    @ApiModelProperty("授权范围")
    @Length(max = 256, message = "编码长度不能超过256")
    private String scope;

    /**
     * 授权类型
     */
    @Size(max = 256, message = "编码长度不能超过256")
    @ApiModelProperty("授权类型")
    @Length(max = 256, message = "编码长度不能超过256")
    @TableField(typeHandler = JsonStringArrayTypeHandler.class)
    private String[] authorizedGrantTypes;

    /**
     * 回调地址
     */
    @Size(max = 256, message = "编码长度不能超过256")
    @ApiModelProperty("回调地址")
    @Length(max = 256, message = "编码长度不能超过256")
    private String webServerRedirectUri;

    /**
     * 权限集合
     */
    @Size(max = 256, message = "编码长度不能超过256")
    @ApiModelProperty("权限集合")
    @Length(max = 256, message = "编码长度不能超过256")
    private String authorities;

    /**
     * 访问令牌有效期（秒）
     */
    @ApiModelProperty("访问令牌有效期（秒）")
    private Integer accessTokenValidity;

    /**
     * 刷新令牌有效期（秒）
     */
    @ApiModelProperty("刷新令牌有效期（秒）")
    private Integer refreshTokenValidity;

    /**
     * 附加信息
     */
    @Size(max = 4096, message = "编码长度不能超过4096")
    @ApiModelProperty("附加信息")
    @Length(max = 4096, message = "编码长度不能超过4,096")
    private String additionalInformation;

    /**
     * 自动授权
     */
    @Size(max = 256, message = "编码长度不能超过256")
    @ApiModelProperty("自动授权")
    @Length(max = 256, message = "编码长度不能超过256")
    private String autoapprove;

    /**
     * 删除标记，0未删除，1已删除
     */
    @ApiModelProperty("删除标记，0未删除，1已删除")
    private String delFlag;

    /**
     * 创建人
     */
    @NotBlank(message = "[创建人]不能为空")
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("创建人")
    @Length(max = 64, message = "编码长度不能超过64")
    private String createBy;

    /**
     * 修改人
     */
    @NotBlank(message = "[修改人]不能为空")
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("修改人")
    @Length(max = 64, message = "编码长度不能超过64")
    private String updateBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private Date updateTime;
}
