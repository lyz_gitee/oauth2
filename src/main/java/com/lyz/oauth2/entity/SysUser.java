package com.lyz.oauth2.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户表
 *
 * @TableName sys_user
 */

@Data
@TableName(value = "sys_user", autoResultMap = true)
public class SysUser implements Serializable {

    /**
     * ID
     */
    @NotNull(message = "[ID]不能为空")
    @ApiModelProperty("ID")
    @TableId(value = "user_id", type = IdType.ASSIGN_ID)
    private String userId;

    /**
     * 用户名
     */
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("用户名")
    @Length(max = 64, message = "编码长度不能超过64")
    private String username;
    /**
     * 密码
     */
    @Size(max = 255, message = "编码长度不能超过255")
    @ApiModelProperty("密码")
    @Length(max = 255, message = "编码长度不能超过255")
    private String password;

    /**
     * 盐值
     */
    @Size(max = 255, message = "编码长度不能超过255")
    @ApiModelProperty("盐值")
    @Length(max = 255, message = "编码长度不能超过255")
    private String salt;
    /**
     * 电话号码
     */
    @Size(max = 20, message = "编码长度不能超过20")
    @ApiModelProperty("电话号码")
    @Length(max = 20, message = "编码长度不能超过20")
    private String phone;
    /**
     * 头像
     */
    @Size(max = 255, message = "编码长度不能超过255")
    @ApiModelProperty("头像")
    @Length(max = 255, message = "编码长度不能超过255")
    private String avatar;
    /**
     * 昵称
     */
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("昵称")
    @Length(max = 64, message = "编码长度不能超过64")
    private String nickname;
    /**
     * 姓名
     */
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("姓名")
    @Length(max = 64, message = "编码长度不能超过64")
    private String name;
    /**
     * 邮箱地址
     */
    @Size(max = 128, message = "编码长度不能超过128")
    @ApiModelProperty("邮箱地址")
    @Length(max = 128, message = "编码长度不能超过128")
    private String email;
    /**
     * 所属部门ID
     */
    @ApiModelProperty("所属部门ID")
    private String deptId;
    /**
     * 创建人
     */
    @NotBlank(message = "[创建人]不能为空")
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("创建人")
    @Length(max = 64, message = "编码长度不能超过64")
    private String createBy;
    /**
     * 修改人
     */
    @NotBlank(message = "[修改人]不能为空")
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("修改人")
    @Length(max = 64, message = "编码长度不能超过64")
    private String updateBy;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private LocalDateTime updateTime;
    /**
     * 锁定标记，0未锁定，9已锁定
     */
    @ApiModelProperty("锁定标记，0未锁定，9已锁定")
    private String locked;

    /**
     * 过期标记，0未过期，1已过期
     */
    @ApiModelProperty("过期标记，0未过期，1已过期")
    private String expired;

    /**
     * 证书过期标记，0未过期，1已过期
     */
    @ApiModelProperty("证书过期标记，0未过期，1已过期")
    private String credentialsExpired;

    /**
     * 禁用标记，0未禁用，1已禁用
     */
    @ApiModelProperty("禁用标记，0未禁用，1已禁用")
    private String disabled;

    /**
     * 删除标记，0未删除，1已删除
     */
    @ApiModelProperty("删除标记，0未删除，1已删除")
    private String deleted;

    /**
     * 微信登录openId
     */
    @Size(max = 32, message = "编码长度不能超过32")
    @ApiModelProperty("微信登录openId")
    @Length(max = 32, message = "编码长度不能超过32")
    private String wxOpenid;
    /**
     * 小程序openId
     */
    @Size(max = 32, message = "编码长度不能超过32")
    @ApiModelProperty("小程序openId")
    @Length(max = 32, message = "编码长度不能超过32")
    private String miniOpenid;
    /**
     * QQ openId
     */
    @Size(max = 32, message = "编码长度不能超过32")
    @ApiModelProperty("QQ openId")
    @Length(max = 32, message = "编码长度不能超过32")
    private String qqOpenid;
    /**
     * 码云标识
     */
    @Size(max = 100, message = "编码长度不能超过100")
    @ApiModelProperty("码云标识")
    @Length(max = 100, message = "编码长度不能超过100")
    private String giteeLogin;
    /**
     * 开源中国标识
     */
    @Size(max = 100, message = "编码长度不能超过100")
    @ApiModelProperty("开源中国标识")
    @Length(max = 100, message = "编码长度不能超过100")
    private String oscId;


}
