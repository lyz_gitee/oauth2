package com.lyz.oauth2.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
* 系统角色表
* @TableName sys_role
*/

@Data
@TableName(value = "sys_role", autoResultMap = true)
public class SysRole implements Serializable {

    /**
     * ID
     */
    @NotNull(message = "[ID]不能为空")
    @ApiModelProperty("ID")
    @TableId(value = "role_id", type = IdType.ASSIGN_ID)
    private String roleId;

    /**
     * 角色名称
     */
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("角色名称")
    @Length(max = 64, message = "编码长度不能超过64")
    private String roleName;
    /**
     * 角色编码
     */
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("角色编码")
    @Length(max = 64, message = "编码长度不能超过64")
    private String roleCode;
    /**
     * 角色描述
     */
    @Size(max = 255, message = "编码长度不能超过255")
    @ApiModelProperty("角色描述")
    @Length(max = 255, message = "编码长度不能超过255")
    private String roleDesc;
    /**
     * 创建人
     */
    @NotBlank(message = "[创建人]不能为空")
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("创建人")
    @Length(max = 64, message = "编码长度不能超过64")
    private String createBy;
    /**
     * 修改人
     */
    @NotBlank(message = "[修改人]不能为空")
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("修改人")
    @Length(max = 64, message = "编码长度不能超过64")
    private String updateBy;
    /**
     * 创建时间
     */
    @NotNull(message = "[创建时间]不能为空")
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private Date updateTime;
    /**
     * 删除标记，0未删除，1已删除
     */
    @ApiModelProperty("删除标记，0未删除，1已删除")
    private String deleted;
}