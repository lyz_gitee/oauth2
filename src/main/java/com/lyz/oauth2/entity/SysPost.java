package com.lyz.oauth2.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * 岗位信息表
 *
 * @TableName sys_post
 */

@Data
@TableName(value = "sys_post", autoResultMap = true)
public class SysPost implements Serializable {

    /**
     * ID
     */
    @NotNull(message = "[ID]不能为空")
    @ApiModelProperty("ID")
    @TableId(value = "post_id", type = IdType.ASSIGN_ID)
    private String postId;

    /**
     * 岗位编码
     */
    @NotBlank(message = "[岗位编码]不能为空")
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("岗位编码")
    @Length(max = 64, message = "编码长度不能超过64")
    private String postCode;
    /**
     * 岗位名称
     */
    @NotBlank(message = "[岗位名称]不能为空")
    @Size(max = 50, message = "编码长度不能超过50")
    @ApiModelProperty("岗位名称")
    @Length(max = 50, message = "编码长度不能超过50")
    private String postName;
    /**
     * 岗位排序
     */
    @NotNull(message = "[岗位排序]不能为空")
    @ApiModelProperty("岗位排序")
    private Integer postSort;
    /**
     * 岗位描述
     */
    @Size(max = 500, message = "编码长度不能超过500")
    @ApiModelProperty("岗位描述")
    @Length(max = 500, message = "编码长度不能超过500")
    private String remark;
    /**
     * 是否删除  -1：已删除  0：正常
     */
    @NotNull(message = "[是否删除  -1：已删除  0：正常]不能为空")
    @ApiModelProperty("是否删除  -1：已删除  0：正常")
    private String deleted;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
     * 创建人
     */
    @NotBlank(message = "[创建人]不能为空")
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("创建人")
    @Length(max = 64, message = "编码长度不能超过64")
    private String createBy;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private Date updateTime;
    /**
     * 更新人
     */
    @NotBlank(message = "[更新人]不能为空")
    @Size(max = 64, message = "编码长度不能超过64")
    @ApiModelProperty("更新人")
    @Length(max = 64, message = "编码长度不能超过64")
    private String updateBy;
}