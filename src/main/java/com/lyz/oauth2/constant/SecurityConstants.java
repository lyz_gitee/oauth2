package com.lyz.oauth2.constant;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/10/27 19:31
 */
public class SecurityConstants {

    /**
     * 角色前缀
     */
    public static final String ROLE = "ROLE_";
    /**
     * 登录方式——账号密码登录
     */
    public static final String OAUTH_GRANT_TYPE_USERNAME_PASSWORD = "username_password";

    /**
     * 自定义 grant type —— 短信验证码 —— 手机号的key
     */
    public static final String OAUTH_PARAMETER_NAME_MOBILE = "mobile";

    /**
     * 自定义 grant type —— 短信验证码 —— 手机号的key
     */
    public static final String OAUTH_GRANT_TYPE_MOBILE = "mobile";


    /**
     * 自定义 grant type —— 短信验证码 —— 短信验证码的key
     */
    public static final String OAUTH_PARAMETER_NAME_SMS_CAPTCHA = "smsCaptcha";


    /**
     * 自定义 grant type —— 账号密码登录 —— 账户名的key
     */
    public static final String OAUTH_PARAMETER_NAME_USERNAME = "username";


    /**
     * 自定义 grant type —— 账号密码登录 —— 账户名的密码
     */
    public static final String OAUTH_PARAMETER_NAME_PASSWORD = "password";


    public static final String DETAILS_LICENSE = "license";


    public static final String PROJECT_LICENSE = "https://wangyuanju.com";

    /**
     * 内部
     */
    public static final String FROM_IN = "Y";

    /**
     * 标志
     */
    public static final String FROM = "from";

    /**
     * 请求header
     */
    public static final String HEADER_FROM_IN = FROM + "=" + FROM_IN;
    /**
     * 客户端ID
     */
    public static final String CLIENT_ID = "clientId";
}
