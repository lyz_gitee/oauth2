package com.lyz.oauth2.extension.server.base;

import cn.hutool.core.util.StrUtil;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.web.authentication.AuthenticationConverter;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/11/2 13:51
 */

public abstract class BaseGrantAuthenticationConverter implements AuthenticationConverter {

    static final String ACCESS_TOKEN_REQUEST_ERROR_URI = "https://datatracker.ietf.org/doc/html/rfc6749#section-5.2";

    protected void throwError(String errorCode, String parameterName, String errorUri) {
        if (StrUtil.isBlank(errorUri)) {
            errorUri = ACCESS_TOKEN_REQUEST_ERROR_URI;
        }
        OAuth2Error error = new OAuth2Error(errorCode, "OAuth 2.0 Parameter: " + parameterName, errorUri);
        throw new OAuth2AuthenticationException(error);
    }
}
