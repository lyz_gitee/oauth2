package com.lyz.oauth2.extension.server.base;

import cn.hutool.extra.spring.SpringUtil;
import com.lyz.oauth2.exception.OauthMessageCodes;
import com.lyz.oauth2.util.ScopeException;
import com.lyz.oauth2.util.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.oidc.endpoint.OidcParameterNames;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2TokenType;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;
import org.springframework.util.ObjectUtils;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/11/2 13:58
 */
@Slf4j
public abstract class BaseGrantAuthenticationProvider implements AuthenticationProvider {

    protected final MessageSourceAccessor messages;

    protected final OAuth2TokenGenerator<?> tokenGenerator;

    protected final AuthenticationManager authenticationManager;

    protected final OAuth2AuthorizationService authorizationService;

    protected static final OAuth2TokenType ID_TOKEN_TOKEN_TYPE = new OAuth2TokenType(OidcParameterNames.ID_TOKEN);


    public BaseGrantAuthenticationProvider(OAuth2TokenGenerator<?> tokenGenerator,
                                           AuthenticationManager authenticationManager,
                                           OAuth2AuthorizationService authorizationService) {
        this.tokenGenerator = tokenGenerator;
        this.authenticationManager = authenticationManager;
        this.authorizationService = authorizationService;
        this.messages = new MessageSourceAccessor(SpringUtil.getBean("securityMessageSource"), Locale.CHINA);
    }

    protected static final String ERROR_URI = "https://datatracker.ietf.org/doc/html/rfc6749#section-5.2";

    /**
     * 获取认证过的scope
     *
     * @param registeredClient 客户端
     * @param requestedScopes  请求中的scope
     * @return 认证过的scope
     */
    protected Set<String> getAuthorizedScopes(RegisteredClient registeredClient, Set<String> requestedScopes) {
        // Default to configured scopes
        Set<String> authorizedScopes = registeredClient.getScopes();
        if (!ObjectUtils.isEmpty(requestedScopes)) {
            Set<String> unauthorizedScopes = requestedScopes.stream()
                    .filter(requestedScope -> !registeredClient.getScopes().contains(requestedScope))
                    .collect(Collectors.toSet());
            if (!ObjectUtils.isEmpty(unauthorizedScopes)) {
                if (!ObjectUtils.isEmpty(unauthorizedScopes)) {
                    SecurityUtils.throwError(
                            OAuth2ErrorCodes.INVALID_REQUEST,
                            "OAuth 2.0 Parameter: " + OAuth2ParameterNames.SCOPE,
                            ERROR_URI);
                }
            }

            authorizedScopes = new LinkedHashSet<>(requestedScopes);
        }

        if (log.isTraceEnabled()) {
            log.trace("Validated token request parameters");
        }
        return authorizedScopes;
    }

    protected OAuth2AuthenticationException oAuth2AuthenticationCommonException(AuthenticationException authenticationException) {
        if (authenticationException instanceof LockedException) {
            return new OAuth2AuthenticationException(new OAuth2Error(com.lyz.oauth2.exception.OAuth2ErrorCodes.USER_LOCKED, this.messages
                    .getMessage(OauthMessageCodes.OAUTH2_LOGIN_USER_LOCKED, "User is locked"), ""));
        }
        if (authenticationException instanceof DisabledException) {
            return new OAuth2AuthenticationException(new OAuth2Error(com.lyz.oauth2.exception.OAuth2ErrorCodes.USER_DISABLED,
                    this.messages.getMessage(OauthMessageCodes.OAUTH2_LOGIN_USER_DISABLED, "User is disabled"),
                    ""));
        }
        if (authenticationException instanceof AccountExpiredException) {
            return new OAuth2AuthenticationException(new OAuth2Error(com.lyz.oauth2.exception.OAuth2ErrorCodes.USER_EXPIRED, this.messages
                    .getMessage(OauthMessageCodes.OAUTH2_LOGIN_USER_EXPIRED, "User has expired"), ""));
        }
        if (authenticationException instanceof CredentialsExpiredException) {
            return new OAuth2AuthenticationException(new OAuth2Error(com.lyz.oauth2.exception.OAuth2ErrorCodes.USER_CREDENTIALS_EXPIRED,
                    this.messages.getMessage(OauthMessageCodes.OAUTH2_LOGIN_USER_CREDENTIALS_EXPIRED,
                            "User credentials have expired"),
                    ""));
        }
        if (authenticationException instanceof ScopeException) {
            return new OAuth2AuthenticationException(new OAuth2Error(com.lyz.oauth2.exception.OAuth2ErrorCodes.INVALID_SCOPE,
                    this.messages.getMessage(OauthMessageCodes.OAUTH2_LOGIN_CLIENT_SCOPE_ERROR, "invalid_scope"), ""));
        }
        return null;
    }
}
