package com.lyz.oauth2.extension.server.sms;

import com.lyz.oauth2.constant.SecurityConstants;
import com.lyz.oauth2.extension.server.base.BaseGrantAuthenticationConverter;
import com.lyz.oauth2.util.SecurityUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 短信验证码登录Token转换器
 *
 * @author vains
 */
public class SmsCaptchaGrantAuthenticationConverter extends BaseGrantAuthenticationConverter {

    @Override
    public Authentication convert(HttpServletRequest request) {
        // grant_type (REQUIRED)
        String grantType = request.getParameter(OAuth2ParameterNames.GRANT_TYPE);
        if (!SecurityConstants.OAUTH_GRANT_TYPE_MOBILE.equals(grantType)) {
            return null;
        }

        // 这里目前是客户端认证信息
        Authentication clientPrincipal = SecurityContextHolder.getContext().getAuthentication();

        // 获取请求中的参数
        MultiValueMap<String, String> parameters = SecurityUtils.getParameters(request);

        // scope (OPTIONAL)
        String scope = parameters.getFirst(OAuth2ParameterNames.SCOPE);
        if (StringUtils.hasText(scope) &&
                parameters.get(OAuth2ParameterNames.SCOPE).size() != 1) {
            throwError(OAuth2ErrorCodes.INVALID_REQUEST, OAuth2ParameterNames.SCOPE, "");
        }
        Set<String> requestedScopes = null;
        if (StringUtils.hasText(scope)) {
            requestedScopes = new HashSet<>(
                    Arrays.asList(StringUtils.delimitedListToStringArray(scope, " ")));
        } else {
            requestedScopes = new LinkedHashSet<>();
        }

        // Mobile phone number (REQUIRED)
        String mobile = parameters.getFirst(SecurityConstants.OAUTH_PARAMETER_NAME_MOBILE);
        if (!StringUtils.hasText(mobile) || parameters.get(SecurityConstants.OAUTH_PARAMETER_NAME_MOBILE).size() != 1) {
            throwError(OAuth2ErrorCodes.INVALID_REQUEST, SecurityConstants.OAUTH_PARAMETER_NAME_MOBILE, "");
        }

        // SMS verification code (REQUIRED)
        String smsCaptcha = parameters.getFirst(SecurityConstants.OAUTH_PARAMETER_NAME_SMS_CAPTCHA);
        if (!StringUtils.hasText(smsCaptcha) || parameters.get(SecurityConstants.OAUTH_PARAMETER_NAME_SMS_CAPTCHA).size() != 1) {
            throwError(OAuth2ErrorCodes.INVALID_REQUEST, SecurityConstants.OAUTH_PARAMETER_NAME_SMS_CAPTCHA, "");
        }

        // 提取附加参数
        Map<String, Object> additionalParameters = new HashMap<>();
        parameters.forEach((key, value) -> {
            if (!key.equals(OAuth2ParameterNames.GRANT_TYPE) &&
                    !key.equals(OAuth2ParameterNames.CLIENT_ID)) {
                additionalParameters.put(key, value.get(0));
            }
        });

        // 构建AbstractAuthenticationToken子类实例并返回
        return new SmsCaptchaGrantAuthenticationToken(new AuthorizationGrantType(SecurityConstants.OAUTH_GRANT_TYPE_MOBILE), clientPrincipal, requestedScopes, additionalParameters);
    }
}