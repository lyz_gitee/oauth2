package com.lyz.oauth2.extension;

import com.lyz.oauth2.checker.AuthenticationChecker;
import com.lyz.oauth2.service.UserDetailsServiceImpl;
import com.lyz.oauth2.util.OAuth2Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * 1.用户名密码认证提供者
 * 2.手机验证码认证提供者
 *
 * @author :  liyuzhi
 * @version :  1.0
 * @createDate : 2023/11/2 15:21
 */
@Slf4j
public class DaoUserDetailsAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    /**
     * The plaintext password used to perform
     * {@link PasswordEncoder#matches(CharSequence, String)} on when the user is not found
     * to avoid SEC-2056.
     */
    private static final String USER_NOT_FOUND_PASSWORD = "userNotFoundPassword";

    private PasswordEncoder passwordEncoder;

    /**
     * The password used to perform {@link PasswordEncoder#matches(CharSequence, String)}
     * on when the user is not found to avoid SEC-2056. This is necessary, because some
     * {@link PasswordEncoder} implementations will short circuit if the password is not
     * in a valid format.
     */
    private volatile String userNotFoundEncodedPassword;

    private UserDetailsService userDetailsService;

    private UserDetailsPasswordService userDetailsPasswordService;

    //自定义
    private RedisTemplate<String, Object> redisTemplate;
    //自定义
    private List<AuthenticationChecker> authenticationCheckers;


    public DaoUserDetailsAuthenticationProvider(PasswordEncoder passwordEncoder,
                                                UserDetailsService userDetailsService,
                                                List<AuthenticationChecker> authenticationChecker) {
        setPasswordEncoder(passwordEncoder);
        setUserDetailsService(userDetailsService);
        setRedisTemplate(redisTemplate);
        this.authenticationCheckers = authenticationChecker;
    }

    @Override
    @Transactional
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return super.authenticate(authentication);
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void additionalAuthenticationChecks(UserDetails userDetails,
                                                  UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        Object credentials = authentication.getCredentials();
        if (credentials == null) {
            this.logger.debug("Failed to authenticate since no credentials provided");
            throw new BadCredentialsException(this.messages
                    .getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }
        //自定义
        authenticationCheck(userDetails, authentication);
    }

    @Override
    protected void doAfterPropertiesSet() {
        Assert.notNull(this.userDetailsService, "A UserDetailsService must be set");
    }

    @Override
    protected final UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        prepareTimingAttackProtection();
        try {
            UserDetails loadedUser = this.getUserDetailsService().loadUserByUsername(username);
            if (loadedUser == null) {
                //未找到用户名 并且手机号登录 = 手机号自动注册
                if (OAuth2Utils.isLoginByMobile()) {
                    loadedUser = automaticRegisterIfMobileAbsent(username);
                }
                if (loadedUser == null) {
                    throw new InternalAuthenticationServiceException(
                            "UserDetailsService returned null, which is an interface contract violation");
                }
            }
            return loadedUser;
        } catch (UsernameNotFoundException ex) {
            //未找到用户名 并且手机号登录 = 手机号自动注册
            if (OAuth2Utils.isLoginByMobile()) {
                return automaticRegisterIfMobileAbsent(username);
            }
            mitigateAgainstTimingAttack(authentication);
            throw ex;
        } catch (InternalAuthenticationServiceException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex);
        }
    }


    @Override
    protected Authentication createSuccessAuthentication(Object principal, Authentication authentication,
                                                         UserDetails user) {
        boolean upgradeEncoding = this.userDetailsPasswordService != null
                && this.passwordEncoder.upgradeEncoding(user.getPassword());
        if (upgradeEncoding) {
            String presentedPassword = authentication.getCredentials().toString();
            String newPassword = this.passwordEncoder.encode(presentedPassword);
            user = this.userDetailsPasswordService.updatePassword(user, newPassword);
        }
        return super.createSuccessAuthentication(principal, authentication, user);
    }

    private void prepareTimingAttackProtection() {
        if (this.userNotFoundEncodedPassword == null) {
            this.userNotFoundEncodedPassword = this.passwordEncoder.encode(USER_NOT_FOUND_PASSWORD);
        }
    }

    private void mitigateAgainstTimingAttack(UsernamePasswordAuthenticationToken authentication) {
        if (authentication.getCredentials() != null) {
            String presentedPassword = authentication.getCredentials().toString();
            this.passwordEncoder.matches(presentedPassword, this.userNotFoundEncodedPassword);
        }
    }

    /**
     * Sets the PasswordEncoder instance to be used to encode and validate passwords. If
     * not set, the password will be compared using
     * {@link PasswordEncoderFactories#createDelegatingPasswordEncoder()}
     *
     * @param passwordEncoder must be an instance of one of the {@code PasswordEncoder}
     *                        types.
     */
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        Assert.notNull(passwordEncoder, "passwordEncoder cannot be null");
        this.passwordEncoder = passwordEncoder;
        this.userNotFoundEncodedPassword = null;
    }

    protected PasswordEncoder getPasswordEncoder() {
        return this.passwordEncoder;
    }

    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    protected UserDetailsService getUserDetailsService() {
        return this.userDetailsService;
    }

    public void setUserDetailsPasswordService(UserDetailsPasswordService userDetailsPasswordService) {
        this.userDetailsPasswordService = userDetailsPasswordService;
    }


    //=====自定义代码==========================================================================================================================

    //认证检查
    private void authenticationCheck(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) {
        for (AuthenticationChecker authenticationChecker : authenticationCheckers) {
            authenticationChecker.check(userDetails, authentication);
        }
    }

    /**
     * 手机号登录的情况下，如果手机号不存在在直接注册
     *
     * @param username 登录账号
     * @return 用户信息
     */
    private UserDetails automaticRegisterIfMobileAbsent(String username) {
        UserDetailsServiceImpl userDetailsService = (UserDetailsServiceImpl) this.getUserDetailsService();
        return userDetailsService.automaticRegisterByMobile(username);
    }


    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public RedisTemplate<String, Object> getRedisTemplate() {
        return redisTemplate;
    }

    public List<AuthenticationChecker> getAuthenticationCheckers() {
        return authenticationCheckers;
    }

    public void setAuthenticationCheckers(List<AuthenticationChecker> authenticationCheckers) {
        this.authenticationCheckers = authenticationCheckers;
    }

    public void addAuthenticationCheckers(AuthenticationChecker authenticationChecker) {
        if (authenticationCheckers == null) {
            this.authenticationCheckers = new ArrayList<>();
            authenticationCheckers.add(authenticationChecker);
        }
    }
}
