/*
 *
 *      Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lengleng (wangiegie@gmail.com)
 *
 */

package com.lyz.oauth2.vo;

import com.lyz.oauth2.entity.SysPost;
import com.lyz.oauth2.entity.SysRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author lengleng
 * @date 2017/10/29
 */
@Data
@ApiModel(value = "前端用户展示对象")
public class UserVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键")
    private String userId;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 随机盐
     */
    @ApiModelProperty(value = "随机盐")
    private String salt;

    /**
     * 微信openid
     */
    @ApiModelProperty(value = "微信open id")
    private String wxOpenid;

    /**
     * QQ openid
     */
    @ApiModelProperty(value = "qq open id")
    private String qqOpenid;

    /**
     * gitee openid
     */
    @ApiModelProperty(value = "gitee open id")
    private String giteeOpenId;

    /**
     * 开源中国 openid
     */
    @ApiModelProperty(value = "开源中国 open id")
    private String oscOpenId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    /**
     * 0-正常，1-删除
     */
    @ApiModelProperty(value = "删除标记,1:已删除,0:正常")
    private String deleted;

    /**
     * 锁定标记
     */
    @ApiModelProperty(value = "锁定标记,0:正常,9:已锁定")
    private String locked;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String phone;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    private String avatar;

    /**
     * 部门ID
     */
    @ApiModelProperty(value = "所属部门")
    private Long deptId;

    /**
     * 部门名称
     */
    @ApiModelProperty(value = "所属部门名称")
    private String deptName;

    /**
     * 角色列表
     */
    @ApiModelProperty(value = "拥有的角色列表")
    private List<SysRole> roleList;

    /**
     * 岗位列表
     */
    private List<SysPost> postList;

    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    private String nickname;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;

}
