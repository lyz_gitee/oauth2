package com.lyz.oauth2.repository;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import com.lyz.oauth2.entity.SysOauthClientDetails;
import com.lyz.oauth2.exception.OAuth2ErrorCodes;
import com.lyz.oauth2.exception.OauthMessageCodes;
import com.lyz.oauth2.service.SysOauthClientDetailsService;
import com.lyz.oauth2.util.MsgUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.Arrays;
import java.util.Optional;

/**
 * @author :  liyuzhi
 * @version :  1.0
 * @description :  描述
 * @createDate : 2023/10/30 17:13
 */
@RequiredArgsConstructor
public class DefaultRegisteredClientRepository implements RegisteredClientRepository {

    @Resource
    private PasswordEncoder passwordEncoder;


    /**
     * 刷新令牌有效期默认 30 天
     */
    private final static int refreshTokenValiditySeconds = 60 * 60 * 24 * 30;

    /**
     * 请求令牌有效期默认 12 小时
     */
    private final static int accessTokenValiditySeconds = 60 * 60 * 12;

    private final SysOauthClientDetailsService sysOauthClientDetailsService;


    @Override
    public void save(RegisteredClient registeredClient) {

    }

    @Override
    public RegisteredClient findById(String id) {
        return null;
    }

    @Override
    public RegisteredClient findByClientId(String clientId) {


        SysOauthClientDetails clientDetails = Optional.ofNullable(sysOauthClientDetailsService.getClientDetailsById(clientId)).orElseThrow(
                () -> {
                    OAuth2Error error = new OAuth2Error(OAuth2ErrorCodes.INVALID_CLIENT,
                            MsgUtils.getSecurityMessage(OauthMessageCodes.OAUTH2_LOGIN_CLIENT_ID_ERROR, " Client authentication failed：" + OAuth2ErrorCodes.INVALID_CLIENT),
                            "");
                    return new OAuth2AuthenticationException(error);
                }
        );

        String encode = passwordEncoder.encode(clientDetails.getClientSecret());
        RegisteredClient.Builder builder = RegisteredClient.withId(clientDetails.getClientId())
                .clientId(clientDetails.getClientId())
                .clientSecret(encode)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC);

        for (String authorizedGrantType : clientDetails.getAuthorizedGrantTypes()) {
            builder.authorizationGrantType(new AuthorizationGrantType(authorizedGrantType));

        }
        // 回调地址
        Optional.ofNullable(clientDetails.getWebServerRedirectUri())
                .ifPresent(redirectUri -> Arrays.stream(redirectUri.split(StrUtil.COMMA))
                        .filter(StrUtil::isNotBlank)
                        .forEach(builder::redirectUri));

        // scope
        Optional.ofNullable(clientDetails.getScope())
                .ifPresent(scope -> Arrays.stream(scope.split(StrUtil.COMMA))
                        .filter(StrUtil::isNotBlank)
                        .forEach(builder::scope));

        return builder
                .tokenSettings(TokenSettings.builder()
                        .accessTokenFormat(OAuth2TokenFormat.SELF_CONTAINED)
                        .accessTokenTimeToLive(Duration.ofSeconds(
                                Optional.ofNullable(clientDetails.getAccessTokenValidity()).orElse(accessTokenValiditySeconds)))
                        .refreshTokenTimeToLive(Duration.ofSeconds(Optional.ofNullable(clientDetails.getRefreshTokenValidity())
                                .orElse(refreshTokenValiditySeconds)))
                        .reuseRefreshTokens(false)
                        .build())
                .clientSettings(ClientSettings.builder()
                        .requireAuthorizationConsent(!BooleanUtil.toBoolean(clientDetails.getAutoapprove()))
                        .build())
                .build();
    }
}
